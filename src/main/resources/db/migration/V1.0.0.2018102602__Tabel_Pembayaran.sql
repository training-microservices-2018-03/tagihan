create table bank (
  id varchar (255),
  kode varchar (20) not null,
  nama varchar (100) not null,
  primary key (id),
  unique (kode)
);

create table virtual_account (
  id varchar (36),
  id_tagihan varchar (36) not null,
  id_bank varchar (36) not null,
  nomor_virtual_account varchar (255) not null,
  aktif boolean not null,
  primary key (id),
  foreign key (id_tagihan) references tagihan (id),
  foreign key (id_bank) references bank (id)
);

create table pembayaran (
  id varchar (36),
  id_virtual_account varchar (36) not null,
  nilai decimal(19,2) not null,
  waktu_pembayaran timestamp not null,
  referensi varchar (255) not null,
  primary key (id),
  unique (referensi),
  foreign key (id_virtual_account) references virtual_account (id)
);