create table tagihan (
  id varchar (36),
  nomor varchar (50) not null,
  nama varchar (255) not null,
  no_hp varchar (50) not null,
  email varchar (200) not null,
  nilai decimal(19,2) not null,
  lunas boolean not null,
  primary key (id),
  unique (nomor)
);