package com.artivisi.training.microservice201803.tagihan.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data @Builder
public class VaRequest {
    private String nomorTagihan;
    private String nama;
    private BigDecimal nilai;
}
