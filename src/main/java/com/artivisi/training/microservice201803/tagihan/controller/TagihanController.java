package com.artivisi.training.microservice201803.tagihan.controller;

import com.artivisi.training.microservice201803.tagihan.entity.Tagihan;
import com.artivisi.training.microservice201803.tagihan.service.TagihanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/tagihan")
public class TagihanController {

    @Autowired
    private TagihanService tagihanService;

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid Tagihan t) {
        tagihanService.simpan(t);
    }

    @GetMapping("/")
    public Page<Tagihan> get(Pageable page){
        return tagihanService.findAll(page);
    }
}
