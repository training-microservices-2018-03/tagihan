package com.artivisi.training.microservice201803.tagihan.service;

import com.artivisi.training.microservice201803.tagihan.dao.BankDao;
import com.artivisi.training.microservice201803.tagihan.dao.PembayaranDao;
import com.artivisi.training.microservice201803.tagihan.dao.TagihanDao;
import com.artivisi.training.microservice201803.tagihan.dao.VirtualAccountDao;
import com.artivisi.training.microservice201803.tagihan.dto.VaPayment;
import com.artivisi.training.microservice201803.tagihan.dto.VaResponse;
import com.artivisi.training.microservice201803.tagihan.entity.Bank;
import com.artivisi.training.microservice201803.tagihan.entity.Pembayaran;
import com.artivisi.training.microservice201803.tagihan.entity.VirtualAccount;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;

@Service @Transactional
public class KafkaListenerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListenerService.class);

    @Autowired private ObjectMapper objectMapper;
    @Autowired private BankDao bankDao;
    @Autowired private TagihanDao tagihanDao;
    @Autowired private VirtualAccountDao virtualAccountDao;
    @Autowired private PembayaranDao pembayaranDao;

    @KafkaListener(topics = "${kafka.topic.va-response}")
    public void handleVaResponse(String message){
        LOGGER.debug("Terima message di topic {} : [{}]", "va-response", message);
        try {
            VaResponse vaResponse = objectMapper.readValue(message, VaResponse.class);

            bankDao.findById(vaResponse.getBank())
                    .ifPresent(bank -> {
                        tagihanDao.findByNomor(vaResponse.getNomorTagihan())
                                .ifPresent(tagihan -> {
                                    VirtualAccount va = new VirtualAccount();
                                    va.setBank(bank);
                                    va.setTagihan(tagihan);
                                    va.setNomorVirtualAccount(vaResponse.getNomorVirtualAccount());
                                    virtualAccountDao.save(va);
                                });
                    });

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @KafkaListener(topics = "${kafka.topic.va-payment}")
    public void handleVaPayment(String message){
        LOGGER.debug("Terima message di topic {} : [{}]", "va-payment", message);

        try {
            VaPayment vaPayment = objectMapper.readValue(message, VaPayment.class);
            bankDao.findById(vaPayment.getBank())
            .ifPresent(b -> {
                virtualAccountDao.findByNomorVirtualAccountAndBank(vaPayment.getNomorVirtualAccount(), b)
                        .ifPresent(va -> {
                            va.setAktif(false);
                            va.getTagihan().setLunas(true);
                            Pembayaran p = new Pembayaran();
                            p.setVirtualAccount(va);
                            p.setNilai(vaPayment.getNilai());
                            p.setReferensi(vaPayment.getReferensi());
                            p.setWaktuPembayaran(vaPayment.getWaktuTransaksi());
                            pembayaranDao.save(p);
                        });
            });
            ;
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
