package com.artivisi.training.microservice201803.tagihan.dao;

import com.artivisi.training.microservice201803.tagihan.entity.Bank;
import com.artivisi.training.microservice201803.tagihan.entity.VirtualAccount;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface VirtualAccountDao extends PagingAndSortingRepository<VirtualAccount, String> {

    Optional<VirtualAccount> findByNomorVirtualAccountAndBank(String vaNumber, Bank b);
}
