package com.artivisi.training.microservice201803.tagihan.dto;

import lombok.Data;

@Data
public class VaResponse {
    private String nomorTagihan;
    private String bank;
    private String nomorVirtualAccount;
}
