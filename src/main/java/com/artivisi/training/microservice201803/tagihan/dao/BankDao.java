package com.artivisi.training.microservice201803.tagihan.dao;

import com.artivisi.training.microservice201803.tagihan.entity.Bank;
import org.springframework.data.repository.CrudRepository;

public interface BankDao extends CrudRepository<Bank, String> {
}
