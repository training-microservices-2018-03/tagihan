package com.artivisi.training.microservice201803.tagihan.dao;

import com.artivisi.training.microservice201803.tagihan.entity.Tagihan;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface TagihanDao extends PagingAndSortingRepository<Tagihan, String> {
    Optional<Tagihan> findByNomor(String nomor);
}
