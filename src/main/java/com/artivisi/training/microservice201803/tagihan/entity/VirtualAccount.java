package com.artivisi.training.microservice201803.tagihan.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity @Data
public class VirtualAccount {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne @JoinColumn(name = "id_bank")
    private Bank bank;

    @ManyToOne @JoinColumn(name = "id_tagihan")
    private Tagihan tagihan;

    @NotBlank
    private String nomorVirtualAccount;

    @NotNull
    private Boolean aktif = true;
}
