package com.artivisi.training.microservice201803.tagihan.service;

import com.artivisi.training.microservice201803.tagihan.dao.TagihanDao;
import com.artivisi.training.microservice201803.tagihan.dto.VaRequest;
import com.artivisi.training.microservice201803.tagihan.entity.Tagihan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service @Transactional
public class TagihanService {
    @Autowired private TagihanDao tagihanDao;
    @Autowired private KafkaSenderService kafkaSenderService;

    public void simpan(Tagihan tagihan) {
        tagihanDao.save(tagihan);
        VaRequest vaRequest = VaRequest.builder()
                .nama(tagihan.getNama())
                .nilai(tagihan.getNilai())
                .nomorTagihan(tagihan.getNomor())
                .build();
        kafkaSenderService.kirimVaRequest(vaRequest);
    }

    public Page<Tagihan> findAll(Pageable page){
        return tagihanDao.findAll(page);
    }
}
