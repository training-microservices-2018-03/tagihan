package com.artivisi.training.microservice201803.tagihan.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity @Data
public class Tagihan {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotBlank
    private String nomor;

    @NotBlank
    private String nama;

    @NotBlank
    private String email;

    @NotBlank
    private String noHp;

    @NotNull @Min(1)
    private BigDecimal nilai;

    private Boolean lunas = false;
}
