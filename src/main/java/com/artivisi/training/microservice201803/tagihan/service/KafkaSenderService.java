package com.artivisi.training.microservice201803.tagihan.service;

import com.artivisi.training.microservice201803.tagihan.dto.VaRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSenderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSenderService.class);

    @Autowired private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired private ObjectMapper objectMapper;

    @Value("${kafka.topic.va-request}") private String topicVaRequest;

    public void kirimVaRequest(VaRequest vaRequest) {
        try {
            String json = objectMapper.writeValueAsString(vaRequest);
            LOGGER.debug("Kirim VA Request : [{}]", json);
            kafkaTemplate.send(topicVaRequest, json);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);

        }
    }
}
