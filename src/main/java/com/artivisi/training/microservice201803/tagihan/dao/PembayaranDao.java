package com.artivisi.training.microservice201803.tagihan.dao;

import com.artivisi.training.microservice201803.tagihan.entity.Pembayaran;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PembayaranDao extends PagingAndSortingRepository<Pembayaran, String> {
}
